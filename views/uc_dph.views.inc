<?php

/**
 * Implementation of hook_views_handlers().
 */
function uc_dph_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'uc_dph') .'/views',
    ),
    'handlers' => array(
      'uc_dph_handler_field_price' => array(
        'parent' => 'uc_product_handler_field_price',
      ),
    ),
  );
}

function uc_dph_views_data_alter(&$data) {
  $data['uc_products']['sell_price']['field']['handler'] = 'uc_dph_handler_field_price';
}