<?php

/**
 * Return a formatted price value to display in the View.
 */
class uc_dph_handler_field_price extends uc_product_handler_field_price {
  function render($values) {
    if ($this->options['format'] == 'numeric') {
      return parent::render($values);
    }

    if ($this->options['format'] == 'uc_price') {
      return uc_dph_prices_for_views($values->{$this->field_alias});
    }
  }
}